using System;
using System.Data;
using Fijo.Infrastructure.DesignPattern.Store;

namespace Fijo.Time.Infrastructure.Store {
	public class DefaultTimeStore : BasicStoreBase<DateTime> {
		#region Overrides of BasicStoreBase<DateTime>
		public override void Store(DateTime entity) {
			throw new ReadOnlyException();
		}

		public override DateTime Get() {
			return DateTime.Now;
		}
		#endregion
	}
}