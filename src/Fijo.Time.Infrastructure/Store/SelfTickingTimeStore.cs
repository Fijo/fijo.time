using System;
using System.Threading;
using Fijo.Infrastructure.DesignPattern.Store;
using Fijo.Time.Infrastructure.Factory;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;
using JetBrains.Annotations;

namespace Fijo.Time.Infrastructure.Store {
	public class SelfTickingTimeStore : BasicStoreBase<DateTime> {
		private DateTime _dateTime = Kernel.Resolve<IConfigurationService>().Get<DateTime, SelfTickingTimeStore>("InitialDateTime");
		private readonly ITickThreadFactory _tickThreadFactory = Kernel.Resolve<ITickThreadFactory>();

		public SelfTickingTimeStore() {
			CreateThread().Start();
		}

		[NotNull]
		private Thread CreateThread() {
			return _tickThreadFactory.CreateThread(() => _dateTime, value => _dateTime = value);
		}

		#region Overrides of BasicStoreBase<DateTime>
		public override void Store(DateTime entity) {
			_dateTime = entity;
		}

		public override DateTime Get() {
			return _dateTime;
		}
		#endregion
	}
}