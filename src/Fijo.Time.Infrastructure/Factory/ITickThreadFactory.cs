using System;
using System.Threading;
using JetBrains.Annotations;

namespace Fijo.Time.Infrastructure.Factory {
	public interface ITickThreadFactory {
		[NotNull, Pure]
		Thread CreateThread([NotNull] Func<DateTime> getter, [NotNull] Action<DateTime> setter);
	}
}