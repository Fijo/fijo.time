using System;
using System.Threading;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;
using JetBrains.Annotations;

namespace Fijo.Time.Infrastructure.Factory {
	public class TickThreadFactory : ITickThreadFactory {
		private readonly long _intervalInTicks = Kernel.Resolve<IConfigurationService>().Get<long, TickThreadFactory>("TickThread.IntervalInTicks");

		public Thread CreateThread(Func<DateTime> getter, Action<DateTime> setter) {
			return new Thread(TickThread(getter, setter))
			{
				Priority = ThreadPriority.Highest,
				IsBackground = true,
				Name = string.Format("{0}.TickThread", typeof (TickThreadFactory).FullName)
			};
		}

		[NotNull]
		private ThreadStart TickThread([NotNull] Func<DateTime> getter, [NotNull] Action<DateTime> setter) {
			return () => {
				var intervalInTicks = _intervalInTicks;
				var timeSpan = new TimeSpan(intervalInTicks);
				while (true) {
					Thread.Sleep(timeSpan);
					setter(getter().AddTicks(intervalInTicks));
				}
			};
		}
	}
}