using System;

namespace Fijo.Time.Infrastructure.Exceptions {
	[Serializable]
	public class TimeRequestException : Exception {
		public TimeRequestException() {}

		public TimeRequestException(string message) : base(message) {}

		public TimeRequestException(string message, Exception innerException) : base(message, innerException) {}
	}
}