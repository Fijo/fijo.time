using System;
using FijoCore.Infrastructure.LightContrib.Default.Service.Try;

namespace Fijo.Time.Infrastructure.Exceptions {
	[Serializable]
	public class TimeMaxRequestTriesException : ExMaxRetriesReachedException<TimeRequestException> {
		public TimeMaxRequestTriesException() {}

		public TimeMaxRequestTriesException(string message, int retryLimit = default(int), Exception innerException = null) : base(message, retryLimit, innerException) {}
	}
}