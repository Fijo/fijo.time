using System;

namespace Fijo.Time.Infrastructure.Exceptions {
	[Serializable]
	public class TimeRequestSocketException : TimeRequestException {
		public TimeRequestSocketException() {}

		public TimeRequestSocketException(string message) : base(message) {}

		public TimeRequestSocketException(string message, Exception innerException) : base(message, innerException) {}
	}
}