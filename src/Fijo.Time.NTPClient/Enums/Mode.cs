/*
* NTPClient
* Copyright (C)2001 Valer BOCAN <vbocan@dataman.ro>
* Copyright (C)2012 Jonas Fischer <fijo.com@googlemail.com>
*
* License
* �This article, along with any associated source code and files, is licensed under The Code Project Open License (CPOL)�
* as written in http://www.codeproject.com/Articles/1005/SNTP-Client-in-C
* 
* This code is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY, without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* 
* To fully understand the concepts used herein, I strongly
* recommend that you read the RFC 2030.
*/

using Fijo.Infrastructure.Documentation.Attributes.Info;

namespace Fijo.Time.NTPClient.Enums {
	[Desc("Mode field values")]
	public enum Mode : byte
	{
		[Desc("0, 6, 7 - Reserved")]
		Unknown = 0,
		SymmetricActive = 1,
		SymmetricPassive = 2,
		Client = 3,
		Server =  4,
		Broadcast = 5
	}
}