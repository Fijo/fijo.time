using System;
using Fijo.Time.Infrastructure.Exceptions;

namespace Fijo.Time.NTPClient.Exceptions {
	[Serializable]
	public class NTPServerSocketException : TimeRequestSocketException {
		public NTPServerSocketException() {}
		public NTPServerSocketException(string message) : base(message) {}
		public NTPServerSocketException(string message, Exception innerException) : base(message, innerException) {}
	}
}