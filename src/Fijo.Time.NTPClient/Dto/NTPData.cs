using System;
using System.ComponentModel;
using Fijo.Infrastructure.Documentation.Attributes.DesignPattern;
using JetBrains.Annotations;

namespace Fijo.Time.NTPClient.Dto {
	[Dto]
	public class NTPData {
		[NotNull, Description("as described in RFC 2030")]
		public byte[] Content = new byte[NTPDataLength];
		public const byte NTPDataLength = 48;
		public DateTime ReceptionTimestamp;
	}
}