﻿/*
* NTPClient
* Copyright (C)2001 Valer BOCAN <vbocan@dataman.ro>
* Copyright (C)2012 Jonas Fischer <fijo.com@googlemail.com>
*
* License
* ´This article, along with any associated source code and files, is licensed under The Code Project Open License (CPOL)´
* as written in http://www.codeproject.com/Articles/1005/SNTP-Client-in-C
* 
* This code is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY, without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* 
* To fully understand the concepts used herein, I strongly
* recommend that you read the RFC 2030.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Fijo.Infrastructure.DesignPattern.Store;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Time.Infrastructure.Exceptions;
using Fijo.Time.NTPClient.Dto;
using Fijo.Time.NTPClient.Enums;
using Fijo.Time.NTPClient.Exceptions;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Module.Number.Interface;
using JetBrains.Annotations;

namespace Fijo.Time.NTPClient.Services {
	/// <summary>
    /// NTPClient is a C# class designed to connect to time servers on the Internet.
    /// The implementation of the protocol is based on the RFC 2030.
    /// 
    /// Public class members:
	///
	/// LeapIndicator - Warns of an impending leap second to be inserted/deleted in the last
	/// minute of the current day. (See the _LeapIndicator enum)
	/// 
	/// VersionNumber - Version number of the protocol (3 or 4).
	/// 
	/// Mode - Returns mode. (See the _Mode enum)
	/// 
	/// Stratum - Stratum of the clock. (See the _Stratum enum)
	/// 
	/// PollInterval - Maximum interval between successive messages.
	/// 
	/// Precision - Precision of the clock.
	/// 
	/// RootDelay - Round trip time to the primary reference source.
	/// 
	/// RootDispersion - Nominal error relative to the primary reference source.
	/// 
	/// ReferenceID - Reference identifier (either a 4 character string or an IP address).
	/// 
	/// ReferenceTimestamp - The time at which the clock was last set or corrected.
	/// 
	/// OriginateTimestamp - The time at which the request departed the client for the server.
	/// 
	/// ReceiveTimestamp - The time at which the request arrived at the server.
	/// 
	/// Transmit Timestamp - The time at which the reply departed the server for client.
	/// 
	/// RoundTripDelay - The time between the departure of request and arrival of reply.
	/// 
	/// LocalClockOffset - The offset of the local clock relative to the primary reference
	/// source.
	/// 
	/// Initialize - Sets up data structure and prepares for connection.
	/// 
	/// Connect - Connects to the time server and populates the data structure.
	///	It can also set the system time.
	/// 
	/// IsResponseValid - Returns true if received data is valid and if comes from
	/// a NTP-compliant time server.
	/// 
	/// ToString - Returns a string representation of the object.
	/// 
	/// -----------------------------------------------------------------------------
    /// Structure of the standard NTP header (as described in RFC 2030)
    ///                       1                   2                   3
    ///   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |LI | VN  |Mode |    Stratum    |     Poll      |   Precision   |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                          Root Delay                           |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                       Root Dispersion                         |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                     Reference Identifier                      |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                                                               |
    ///  |                   Reference Timestamp (64)                    |
    ///  |                                                               |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                                                               |
    ///  |                   Originate Timestamp (64)                    |
    ///  |                                                               |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                                                               |
    ///  |                    Receive Timestamp (64)                     |
    ///  |                                                               |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                                                               |
    ///  |                    Transmit Timestamp (64)                    |
    ///  |                                                               |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                 Key Identifier (optional) (32)                |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    ///  |                                                               |
    ///  |                                                               |
    ///  |                 Message Digest (optional) (128)               |
    ///  |                                                               |
    ///  |                                                               |
    ///  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
    /// 
    /// -----------------------------------------------------------------------------
    /// 
    /// NTP Timestamp Format (as described in RFC 2030)
    ///                         1                   2                   3
	///     0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	/// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	/// |                           Seconds                             |
	/// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	/// |                  Seconds Fraction (0-padded)                  |
	/// +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	/// 
    /// </summary>
	public class NTPClient : INTPClient {
		private readonly INumberConcat _numberConcat = Kernel.Resolve<INumberConcat>();

		#region Offset constants for timestamps in the data structure
		private const byte OffReferenceID = 12;
		private const byte OffReferenceTimestamp = 16;
		private const byte OffOriginateTimestamp = 24;
		private const byte OffReceiveTimestamp = 32;
		private const byte OffTransmitTimestamp = 40;
		#endregion

		public LeapIndicator GetLeapIndicator(NTPData data) {
			var val = GetLeapIndicatorValue(data);
			if(val > 3) val = 3;
			return (LeapIndicator) val;
		}
		
		#region Get Set
		#region GetByte
		[Pure]
		private byte GetHeaderByte(NTPData data) {
			return data.Content[0];
		}

		[Pure, Desc("Isolate bits 0 - 3")]
		private byte GetModeByte(NTPData data) {
			return (byte) (data.Content[0] & 7);
		}
		
		[Pure]
		private byte GetStratumByte(NTPData data) {
			return data.Content[1];
		}
		
		[Pure]
		private byte GetPollIntervalByte(NTPData data) {
			return data.Content[2];
		}
		
		[Pure]
		private double Extract4BytesFromIndex(NTPData data, int startIndex) {
			var content = data.Content;
			return _numberConcat.Concat<byte, double>(Enumerable.Range(startIndex, 4).Select(index => content[index])) / 0x10000;
		}
		#endregion
		
		[Pure, Desc("Isolate the two last (most significant) bits")]
		private byte GetLeapIndicatorValue(NTPData data) {
			return (byte) (GetHeaderByte(data) >> 6);
		}
		
		[Pure]
		public byte GetVersionNumber(NTPData data) {
			return GetNTPVersion(data);
		}
		
		[Pure, Desc("Isolate bits 3 - 5")]
		private byte GetNTPVersion(NTPData data) {
			return (byte) ((GetHeaderByte(data) & 56) >> 3);
		}
		
		[Pure]
		public Mode GetMode(NTPData data) {
			byte val = GetModeByte(data);
			if(!Enum.IsDefined(typeof(Mode), val)) return Mode.Unknown;
			return (Mode) val;
		}
		
		[Pure]
		public Stratum GetStratum(NTPData data) {
			byte val = GetStratumByte(data);
			switch (val) {
				case 0:
					return Stratum.Unspecified;
				case 1:
					return Stratum.PrimaryReference;
				default:
					return val < 16 ? Stratum.SecondaryReference : Stratum.Reserved;
			}
		}
		
		[Pure, Desc("Gets the Pool Cache Update Interval")]
		public uint GetPollInterval(NTPData data) {
			return (uint) Math.Round(Math.Pow(2, GetPollIntervalByte(data)));
		}
		
		[Pure, Desc("in microseconds")]
		public double GetPrecision(NTPData data) {
			return Math.Pow(2, data.Content[3]);
		}
		
		[Pure, Desc("in microseconds")]
		public double GetRootDelay(NTPData data) {
			return Extract4BytesFromIndex(data, 4);
		}
		
		[Pure, Desc("in microseconds")]
		public double GetRootDispersion(NTPData data) {
			return Extract4BytesFromIndex(data, 8);
		}
		
		[Pure]
		public IEnumerable<char> GetReferenceID(NTPData data, IBasicStore<DateTime> timeStore) {
			var stratum = GetStratum(data);
			switch (stratum) {
				case Stratum.Unspecified:
					goto case Stratum.PrimaryReference;
				case Stratum.PrimaryReference:
					return GetReferenceId(data);
				case Stratum.SecondaryReference:
					return GetSecondVersionReferenceId(data, timeStore);
			}
			throw new NotSupportedException(string.Format("Stratum ´{0}´", Enum.GetName(typeof (Stratum), stratum)));
		}
		
		[Pure]
		private IEnumerable<char> GetSecondVersionReferenceId(NTPData data, IBasicStore<DateTime> timeStore) {
			switch (GetVersionNumber(data)) {
				case 3:
					return GetReferenceId(data);
				case 4:
					return GetVersion4ReferenceId(data, timeStore);
				default:
					return GetDefaultReferenceId();
			}
		}
		
		[Pure]
		private IEnumerable<char> GetVersion4ReferenceId(NTPData data, IBasicStore<DateTime> timeStore) {
			var time = GetReferenceTimeToNow(GetMilliSeconds(data, OffReferenceID), timeStore);
			return _numberConcat.Split<long, char>(time.Ticks, 256, 4);
		}
		
		[Pure]
		private IEnumerable<char> GetDefaultReferenceId() {
			return Enumerable.Range(0, 4).Select(x => default(char));
		}
		
		[Pure]
		private IEnumerable<char> GetReferenceId(NTPData data) {
			var content = data.Content;
			return Enumerable.Range(OffReferenceID, 4).Select(index => (char) content[index]);
		}
		
		[Pure]
		public DateTime GetReferenceTimestamp(NTPData data, IBasicStore<DateTime> timeStore) {
			return GetReferenceTimeToNow(GetMilliSeconds(data, OffReferenceTimestamp), timeStore);
		}
		
		[Pure]
		private DateTime GetReferenceTimeToNow(ulong milliseconds, IBasicStore<DateTime> timeStore) {
			var time = ComputeDate(milliseconds);
			// Take care of the time zone
			var offspan = TimeZone.CurrentTimeZone.GetUtcOffset(timeStore.Get());
			return time + offspan;
		}
		
		[Pure]
		public DateTime GetOriginateTimestamp(NTPData data) {
			return ComputeDate(GetMilliSeconds(data, OffOriginateTimestamp));
		}
		
		[Pure]
		public DateTime GetReceiveTimestamp(NTPData data, IBasicStore<DateTime> timeStore) {
			return GetReferenceTimeToNow(GetMilliSeconds(data, OffReceiveTimestamp), timeStore);
		}
		
		[Pure]
		public DateTime GetTransmitTimestamp(NTPData data, IBasicStore<DateTime> timeStore) {
			return GetReferenceTimeToNow(GetMilliSeconds(data, OffTransmitTimestamp), timeStore);
		}

		public void SetTransmitTimestamp(NTPData data, DateTime value) {
			SetDate(data, OffTransmitTimestamp, value);
		}
		
		[Pure, Desc("in milliseconds")]
		public int GetRoundTripDelay(NTPData data, IBasicStore<DateTime> timeStore) {
			var span = (GetReceiveTimestamp(data, timeStore) - GetOriginateTimestamp(data)) + (data.ReceptionTimestamp - GetTransmitTimestamp(data, timeStore));
			return (int) span.TotalMilliseconds;
		}
		
		[Pure, Desc("in milliseconds")]
		public int GetLocalClockOffset(NTPData data, IBasicStore<DateTime> timeStore) {
			var span = (GetReceiveTimestamp(data, timeStore) - GetOriginateTimestamp(data)) - (data.ReceptionTimestamp - GetTransmitTimestamp(data, timeStore));
			return (int) (span.TotalMilliseconds / 2);
		}

		[Pure, Desc("Compute date, given the number of milliseconds since January 1, 1900")]
		private DateTime ComputeDate(ulong milliseconds) {
			var span = TimeSpan.FromMilliseconds(milliseconds);
			var time = new DateTime(1900, 1, 1);
			time += span;
			return time;
		}

		[Desc("Compute the number of milliseconds, given the offset of a 8-byte array")]
		private ulong GetMilliSeconds(NTPData data, byte offset) {
			ulong intpart = 0;
			ulong fractpart = 0;
			var content = data.Content;

			for (var i = 0; i <= 3; i++) {
				intpart = 256 * intpart + content[offset + i];
			}
			for (var i = 4; i <= 7; i++) {
				fractpart = 256 * fractpart + content[offset + i];
			}
			return intpart * 1000 + (fractpart * 1000) / 0x100000000L;
		}

		[Desc("Compute the 8-byte array, given the date")]
		private void SetDate(NTPData data, byte offset, DateTime date) {
			var startOfCentury = new DateTime(1900, 1, 1, 0, 0, 0); // January 1, 1900 12:00 AM
			var content = data.Content;

			var milliseconds = (ulong) (date - startOfCentury).TotalMilliseconds;
			var intpart = milliseconds / 1000;
			var fractpart = ((milliseconds % 1000) * 0x100000000) / 1000;

			var temp = intpart;
			for (var i = 3; i >= 0; i--) {
				content[offset + i] = (byte) (temp % 256);
				temp = temp / 256;
			}

			temp = fractpart;
			for (var i = 7; i >= 4; i--) {
				content[offset + i] = (byte) (temp % 256);
				temp = temp / 256;
			}
		}
		#endregion

		[Desc("Initialize the NTPClient data")]
		private void Initialize(NTPData data, IBasicStore<DateTime> timeStore) {
			var content = data.Content;
			// Set version number to 4 and Mode to 3 (client)
			content[0] = 0x1b;
			// Initialize all other fields with 0
			for (var i = 1; i < 48; i++) {
				content[i] = 0;
			}
			// Initialize the transmit timestamp
			SetTransmitTimestamp(data, timeStore.Get());
		}

		#region Connection
		[Desc("Connect to the time server and update system time")]
		public void Connect(string timeServer, NTPData data, IBasicStore<DateTime> timeStore, bool updateTimeStore = true) {
			try {
				Initialize(data, timeStore);

				var targetPoint = GetTarget(timeServer);
				using (var socket = new UdpClient()) {
					socket.Connect(targetPoint);

					var content = data.Content;
					socket.Send(content, content.Length);
					data.Content = socket.Receive(ref targetPoint);
					socket.Close();
				}
				if (!IsResponseValid(data)) throw new InvalidResponseException(string.Format("Invalid response from {0}", timeServer));
				data.ReceptionTimestamp = timeStore.Get();
			}
			catch (SocketException e) {
				throw new NTPServerSocketException(e.Message, e);
			}

			if(updateTimeStore) UpdateTimeStore(data, timeStore);
		}

		private void UpdateTimeStore(NTPData data, IBasicStore<DateTime> timeStore) {
			timeStore.Store(GetTransmitTimestamp(data, timeStore));
		}

		private IPEndPoint GetTarget(string timeServer) {
			return new IPEndPoint(GetAddress(timeServer).AddressList[0], 123);
		}
		
		private IPHostEntry GetAddress(string timeServer) {
			return Dns.GetHostEntry(timeServer);
		}
		#endregion
		
		[Pure, Desc("Check if the response from server is valid")]
		private bool IsResponseValid(NTPData data) {
			return data.Content.Length >= NTPData.NTPDataLength && GetMode(data) == Mode.Server;
		}

		[Obsolete]
		public string ToString(NTPData data, IBasicStore<DateTime> timeStore) {
			string str;

			str = "Leap Indicator: ";
			switch (GetLeapIndicator(data)) {
				case LeapIndicator.NoWarning:
					str += "No warning";
					break;
				case LeapIndicator.LastMinute61:
					str += "Last minute has 61 seconds";
					break;
				case LeapIndicator.LastMinute59:
					str += "Last minute has 59 seconds";
					break;
				case LeapIndicator.Alarm:
					str += "Alarm Condition (clock not synchronized)";
					break;
			}
			str += "\r\nVersion number: " + GetVersionNumber(data).ToString() + "\r\n";
			str += "Mode: ";
			switch (GetMode(data)) {
				case Mode.Unknown:
					str += "Unknown";
					break;
				case Mode.SymmetricActive:
					str += "Symmetric Active";
					break;
				case Mode.SymmetricPassive:
					str += "Symmetric Pasive";
					break;
				case Mode.Client:
					str += "Client";
					break;
				case Mode.Server:
					str += "Server";
					break;
				case Mode.Broadcast:
					str += "Broadcast";
					break;
			}
			str += "\r\nStratum: ";
			switch (GetStratum(data)) {
				case Stratum.Unspecified:
				case Stratum.Reserved:
					str += "Unspecified";
					break;
				case Stratum.PrimaryReference:
					str += "Primary Reference";
					break;
				case Stratum.SecondaryReference:
					str += "Secondary Reference";
					break;
			}
			str += "\r\nLocal time: " + GetTransmitTimestamp(data, timeStore).ToString();
			str += "\r\nPrecision: " + GetPrecision(data).ToString() + " microseconds";
			str += "\r\nPoll Interval: " + GetPollInterval(data).ToString() + " s";
			str += "\r\nReference ID: " + Encoding.Default.GetString(GetReferenceID(data, timeStore).Select(x => (byte)x).ToArray());
			str += "\r\nRoot Dispersion: " + GetRootDispersion(data).ToString() + " microseconds";
			str += "\r\nRound Trip Delay: " + GetRoundTripDelay(data, timeStore).ToString() + " ms";
			str += "\r\nLocal Clock Offset: " + GetLocalClockOffset(data, timeStore).ToString() + " ms";
			str += "\r\n";

			return str;
		}
	}
}