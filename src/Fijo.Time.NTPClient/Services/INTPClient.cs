using System;
using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Store;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Time.NTPClient.Dto;
using Fijo.Time.NTPClient.Enums;
using JetBrains.Annotations;

namespace Fijo.Time.NTPClient.Services {
	public interface INTPClient {
		LeapIndicator GetLeapIndicator(NTPData data);

		[Pure]
		byte GetVersionNumber(NTPData data);

		[Pure]
		Mode GetMode(NTPData data);

		[Pure]
		Stratum GetStratum(NTPData data);

		[Pure, Desc("Gets the Pool Cache Update Interval")]
		uint GetPollInterval(NTPData data);

		[Pure, Desc("in microseconds")]
		double GetPrecision(NTPData data);

		[Pure, Desc("in microseconds")]
		double GetRootDelay(NTPData data);

		[Pure, Desc("in microseconds")]
		double GetRootDispersion(NTPData data);

		[Pure]
		IEnumerable<char> GetReferenceID(NTPData data, IBasicStore<DateTime> timeStore);

		[Pure]
		DateTime GetReferenceTimestamp(NTPData data, IBasicStore<DateTime> timeStore);

		[Pure]
		DateTime GetOriginateTimestamp(NTPData data);

		[Pure]
		DateTime GetReceiveTimestamp(NTPData data, IBasicStore<DateTime> timeStore);

		[Pure]
		DateTime GetTransmitTimestamp(NTPData data, IBasicStore<DateTime> timeStore);

		void SetTransmitTimestamp(NTPData data, DateTime value);

		[Pure, Desc("in milliseconds")]
		int GetRoundTripDelay(NTPData data, IBasicStore<DateTime> timeStore);

		[Pure, Desc("in milliseconds")]
		int GetLocalClockOffset(NTPData data, IBasicStore<DateTime> timeStore);

		[Desc("Connect to the time server and update system time")]
		void Connect(string timeServer, NTPData data, IBasicStore<DateTime> timeStore, bool updateTimeStore = true);

		[Obsolete]
		string ToString(NTPData data, IBasicStore<DateTime> timeStore);
	}
}