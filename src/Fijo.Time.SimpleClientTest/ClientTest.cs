﻿using System;
using Fijo.Time.NTPClientTest.Properties;
using Fijo.Time.SimpleClient.Interface;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using NUnit.Framework;

namespace Fijo.Time.SimpleClientTest {
	[TestFixture]
	public class ClientTest {
		private ISimpleTimeProvider _simpleTimeProvider;

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
			_simpleTimeProvider = Kernel.Resolve<ISimpleTimeProvider>();
		}

		[Test]
		public void Test() {
			var atLeastTicks = new DateTime(2012, 6, 1).Ticks;
			var ticks = _simpleTimeProvider.GetDate().Ticks;
			Assert.Greater(ticks, atLeastTicks);
		}

		[Test]
		public void GetDateTwoTimes_DifferenceIsNotBiggerThan15Minutes() {
			var timeA = _simpleTimeProvider.GetDate();
			var timeB = _simpleTimeProvider.GetDate();
			var difference = new TimeSpan(Math.Abs(timeB.Ticks - timeA.Ticks));
			var maxDifference = new TimeSpan(0, 15, 0);
			Assert.Less(difference.Ticks, maxDifference.Ticks);
		}
	}
}
