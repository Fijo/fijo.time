using System;

namespace Fijo.Time.SimpleClient.Interface {
	public interface ISimpleTimeProvider {
		DateTime GetDate();
	}
}