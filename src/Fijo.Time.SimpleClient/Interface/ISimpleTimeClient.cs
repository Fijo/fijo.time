using System;

namespace Fijo.Time.SimpleClient.Interface {
	public interface ISimpleTimeClient {
		DateTime GetNTPDate();
	}
}