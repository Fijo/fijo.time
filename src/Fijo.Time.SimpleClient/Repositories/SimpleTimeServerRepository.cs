using System.Collections.Generic;
using Fijo.Infrastructure.DesignPattern.Repository;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Extentions.ICollection.Generic;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;

namespace Fijo.Time.SimpleClient.Repositories {
	public class SimpleTimeServerRepository : RepositoryBase<string> {
		// ToDo make IConfigurationService support collections and used the collection support instead of spliting the string here
		private readonly IList<string> _servers = Kernel.Resolve<IConfigurationService>().Get<string, SimpleTimeServerRepository>("Servers").Split(';');

		#region Overrides of RepositoryBase<string>
		public override string Get() {
			return _servers.ShuffleSingle();
		}
		#endregion
	}
}