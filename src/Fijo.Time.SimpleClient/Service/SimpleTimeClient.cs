using System;
using System.Diagnostics.Contracts;
using System.IO;
using System.Net.Sockets;
using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Infrastructure.Documentation.Attributes.Info;
using Fijo.Time.SimpleClient.Exceptions;
using Fijo.Time.SimpleClient.Interface;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;

namespace Fijo.Time.SimpleClient.Service {
	public class SimpleTimeClient : ISimpleTimeClient {
		private readonly IRepository<string> _serverRepository = Kernel.Resolve<IRepository<string>>();

		public DateTime GetNTPDate() {
			var serverResponse = GetResponse(_serverRepository.Get());
			if (!ValidateSignature(serverResponse)) throw new SimpleTimeInvalidReponseFormatException(string.Format("InvalidSignature �{0}�", serverResponse));
			return GetReponseDateTime(serverResponse);
		}

		[Pure]
		private DateTime GetReponseDateTime(string serverResponse) {
			var jd = IntParse(serverResponse.Substring(1, 5));
			var year = IntParse(serverResponse.Substring(7, 2)) + (GetAdditionalYearCount(jd));
			var month = IntParse(serverResponse.Substring(10, 2));
			var day = IntParse(serverResponse.Substring(13, 2));
			var hour = IntParse(serverResponse.Substring(16, 2));
			var minute = IntParse(serverResponse.Substring(19, 2));
			var second = IntParse(serverResponse.Substring(22, 2));

			return new DateTime(year, month, day, hour, minute, second);
		}

		[Pure]
		protected virtual int IntParse(string value) {
			int result;
			if(!int.TryParse(value, out result)) throw new SimpleTimeInvalidReponseFormatException(string.Format("IntParse faild for �{0}�", value));
			return result;
		}

		[Pure, Desc("cool without writing anything - i realy like this!")]
		private string GetResponse(string server) {
			try {
				var tcpClient = new TcpClient(server, 13);
				using (var reader = new StreamReader(tcpClient.GetStream())) {
					var serverResponse = reader.ReadToEnd();
					reader.Close();
					tcpClient.Close();
					return serverResponse;
				}
			}
			catch (SocketException e) {
				throw new SimpleTimeServerSocketException(e.Message, e);
			}
		}

		[Pure]
		private int GetAdditionalYearCount(int jd) {
			return jd > 51544 ? 2000 : 1999;
		}

		[Pure]
		private bool ValidateSignature(string serverResponse) {
			return serverResponse.Length > 47 && serverResponse.Substring(38, 9) == "UTC(NIST)";
		}
	}
}