﻿using System;
using System.Diagnostics;
using Fijo.Time.Infrastructure.Exceptions;
using Fijo.Time.SimpleClient.Interface;
using FijoCore.Infrastructure.DependencyInjection.InitKernel;
using FijoCore.Infrastructure.LightContrib.Default.Service.CheckName;
using FijoCore.Infrastructure.LightContrib.Default.Service.Try;
using FijoCore.Infrastructure.LightContrib.Module.Configuration;

namespace Fijo.Time.SimpleClient.Service {
	public class SimpleTimeProvider : ISimpleTimeProvider {
		protected readonly ISimpleTimeClient SimpleTimeClient = Kernel.Resolve<ISimpleTimeClient>();
		protected readonly ITry Try = Kernel.Resolve<ITry>();
		protected readonly int MaxTrys;

		public SimpleTimeProvider() {
			MaxTrys = Kernel.Resolve<IConfigurationService>().Get<int, SimpleTimeProvider>(CN.Get(() => MaxTrys));
			Debug.Assert(MaxTrys >= 0);
		}

		public DateTime GetDate() {
			return Try.TryTimes<DateTime, TimeRequestException, TimeMaxRequestTriesException>(InternalGet, MaxTrys);
		}
		
		private DateTime InternalGet() {
			return SimpleTimeClient.GetNTPDate();
		}
	}
}