using Fijo.Infrastructure.DesignPattern.Repository;
using Fijo.Time.Infrastructure.Properties;
using Fijo.Time.SimpleClient.Interface;
using Fijo.Time.SimpleClient.Repositories;
using Fijo.Time.SimpleClient.Service;
using FijoCore.Infrastructure.DependencyInjection.Extentions.IKernel;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Base;
using FijoCore.Infrastructure.LightContrib.Properties;
using FijoCore.Infrastructure.WrappedLibs.DependencyInjection.Interface;

namespace Fijo.Time.SimpleClient.Properties {
	public class SimpleTimeClientInjectionModule : ExtendedNinjectModule {
		public override void AddModule(IKernel kernel) {
			kernel.Load(new LightContribInjectionModule(), new TimeInfrastructureInjectionModule());
		}

		public override void OnLoad(IKernel kernel) {
			kernel.Bind<ISimpleTimeClient>().To<SimpleTimeClient>().InSingletonScope();
			kernel.Bind<ISimpleTimeProvider>().To<SimpleTimeProvider>().InSingletonScope();
			// use injected into
			kernel.Bind<IRepository<string>>().To<SimpleTimeServerRepository>().InSingletonScope();
		}
	}
}