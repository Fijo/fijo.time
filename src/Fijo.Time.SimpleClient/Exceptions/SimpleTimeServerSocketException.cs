using System;
using Fijo.Time.Infrastructure.Exceptions;

namespace Fijo.Time.SimpleClient.Exceptions {
	[Serializable]
	public class SimpleTimeServerSocketException : TimeRequestSocketException {
		public SimpleTimeServerSocketException() {}
		public SimpleTimeServerSocketException(string message) : base(message) {}
		public SimpleTimeServerSocketException(string message, Exception innerException) : base(message, innerException) {}
	}
}