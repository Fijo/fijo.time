using System;
using Fijo.Time.Infrastructure.Exceptions;

namespace Fijo.Time.SimpleClient.Exceptions {
	[Serializable]
	public class SimpleTimeInvalidReponseFormatException : TimeRequestException {
		public SimpleTimeInvalidReponseFormatException() {}
		public SimpleTimeInvalidReponseFormatException(string message) : base(message) {}
		public SimpleTimeInvalidReponseFormatException(string message, Exception innerException) : base(message, innerException) {}
	}
}