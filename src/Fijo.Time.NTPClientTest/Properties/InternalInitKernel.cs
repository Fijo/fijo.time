using Fijo.Time.NTPClient.Properties;
using FijoCore.Infrastructure.DependencyInjection.InitKernel.Init;

namespace Fijo.Time.NTPClientTest.Properties {
	public class InternalInitKernel : ExtendedInitKernel
	{
		public override void PreInit()
		{
			LoadModules(new NTPClientInjectionModule());
		}
	}
}