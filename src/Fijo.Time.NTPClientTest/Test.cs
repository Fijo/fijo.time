﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using Fijo.Time.Infrastructure.Store;
using Fijo.Time.NTPClient.Dto;
using Fijo.Time.NTPClientTest.Properties;
using NUnit.Framework;

namespace Fijo.Time.NTPClientTest {
	[TestFixture]
	public class ClientTest {
		private const string TimeServer = "tick.usno.navy.mil";

		[SetUp]
		public void SetUp() {
			new InternalInitKernel().Init();
		}

		[Test, Ignore]
		public void GetDateTimeStringForADayToUseItInTheConfigFile() {
			Debug.WriteLine(new DateTime(2010, 1, 1).ToString(CultureInfo.InvariantCulture));
		}

		[Test]
		public void Test() {
			Debug.WriteLine(string.Format("Connecting to: {0}\r\n", TimeServer));

			var client = new NTPClient.Services.NTPClient();
			var data = new NTPData();
			var timeStore = new SelfTickingTimeStore();
			client.Connect(TimeServer, data, timeStore);

			var atLeastTicks = new DateTime(2012, 6, 1).Ticks;
			var ticks = timeStore.Get().Ticks;
			Assert.Greater(ticks, atLeastTicks);

			Debug.Write(client.ToString(data, timeStore));
			Thread.Sleep(10080);
			client.Connect(TimeServer, data, timeStore);
			Debug.WriteLine(client.ToString(data, timeStore));
			client.Connect(TimeServer, data, timeStore);
			Debug.WriteLine(client.ToString(data, timeStore));
		}
	}
}